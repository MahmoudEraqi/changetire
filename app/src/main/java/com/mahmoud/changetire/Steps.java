package com.mahmoud.changetire;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Mahmoud on 03/08/2015.
 */
public class Steps extends Activity{
    TextView tv;
    ImageView img;
    int x=0;
    ImageButton ibutton,ibutton2;
Button b;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.steps);
        img = (ImageView) findViewById(R.id.imageView);
        img.setImageResource(R.drawable.step0);
tv=(TextView)findViewById(R.id.textView);
        tv.setText("1-Take out the spare tire and the jack.");
        ibutton = (ImageButton) findViewById(R.id.imageButton2);
        ibutton2 = (ImageButton) findViewById(R.id.imageButton);

    }
    public void next(View v){
        if (x<8) {
            x++;

            String currentImage = "step" + x;
            int resID1 = getResources().getIdentifier(currentImage, "string", getPackageName());
            tv.setText(getString(resID1));

            int resID = getResources().getIdentifier(currentImage, "drawable", getPackageName());
            img.setImageResource(resID);
        }
    }
    public void previous(View v){
        if(x!=0) {
            x--;
        }
        String currentImage="step"+x;
        int resID1 = getResources().getIdentifier(currentImage , "string", getPackageName());
        tv.setText(getString(resID1));

        int resID = getResources().getIdentifier(currentImage , "drawable", getPackageName());
        img.setImageResource(resID);

    }




}



